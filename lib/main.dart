import 'package:app_sjg/pages/guide.dart';
import 'package:app_sjg/pages/login.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(new MaterialApp(
    debugShowCheckedModeBanner: false,
    home: GuidePage(),
    theme: ThemeData(platform: TargetPlatform.iOS,),
    routes:<String,WidgetBuilder>{
       "/login":(BuildContext  context)=> LoginPage(),
       "/guide":(BuildContext  context) => GuidePage(),
    },
  ));
}
