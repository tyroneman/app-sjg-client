import 'dart:ui';

import 'package:app_sjg/utils/color.dart';
import 'package:app_sjg/utils/textstyle.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  final  bool  isLogin;

  HomePage(this.isLogin);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  PageController   pageController = PageController();
  GlobalKey<_HomePageState> _pageIndicatorKey = GlobalKey();
  int  pageIndex = 0;

  Widget  _createPageView(BuildContext  context){
     return SizedBox(
         width: window.physicalSize.width,
         height: 150.0,
       child: PageView(
           controller: pageController,
         onPageChanged: (pageindex) {
           setState(() {
             pageIndex = pageindex;
           });
         },
          children: [
             Container(width: window.physicalSize.width,color: Colors.white,
               padding: EdgeInsets.only(left: 20.0),
               child: Row(
                children: [
                  Expanded(
                      flex:1,
                      child: GestureDetector(
                          child: Text('景区介绍'))),
                  Expanded(
                      flex:1,
                      child: GestureDetector(child: Text('交通指南'))),
                  Expanded(
                      flex:1,
                      child: GestureDetector(child: Text('任务中心'))),
                  Expanded(
                      flex:1,
                      child: GestureDetector(child: Text('客服中心')))
              ],
             ),),
            Container(width: window.physicalSize.width,color: Colors.white,),
        ],
       ),
     );
  }

  Widget  _createView(BuildContext  context){
     return  Stack(
       children: [
         _createPageView(context),
         _createPageIndicator()
       ],
     );
  }

  _dotWidget(int index) {
    return Container(
        width: 10,
        height: 10,
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.all(color: Colors.black, width: 1.0),
            color: (pageIndex == index) ? Colors.black : Colors.transparent));
  }

  _handlePageIndicatorTap(TapUpDetails detail) {
    RenderBox renderBox = _pageIndicatorKey.currentContext.findRenderObject();
    Size widgeSize = renderBox.paintBounds.size;
    Offset tapOffset = renderBox.globalToLocal(detail.globalPosition);

    if (tapOffset.dx > widgeSize.width / 2) {
      _scrollToNextPage();
    } else {
      _scrollToPreviousPage();
    }
  }

  _scrollToPreviousPage() {
    if (pageIndex > 0) {
      pageController.animateToPage(pageIndex - 1,
          duration: Duration(milliseconds: 200), curve: Curves.ease);
    }
  }

  _scrollToNextPage() {
    if (pageIndex > 0) {
      pageController.animateToPage(pageIndex + 1,
          duration: Duration(milliseconds: 200), curve: Curves.ease);
    }
  }


  _createPageIndicator() {
    return Opacity(
      opacity: 0.7,
      child: Align(
        alignment:Alignment.center,
        child: Container(
          margin: EdgeInsets.only(bottom: 10,top: 120.0),
          height: 40,
          width: 80,
          padding: EdgeInsets.all(0),
          decoration: BoxDecoration(
              color: Colors.transparent, //.withAlpha(128),
              borderRadius: BorderRadius.all(const Radius.circular(6.0))),
          child: GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTapUp: (detail) => _handlePageIndicatorTap(detail),
            child: Row(
                key: _pageIndicatorKey,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  _dotWidget(0),
                  _dotWidget(1),
                ]),
          ),
        ),
      ),
    );
  }

  Widget   ADBanner(){
      return  Container(
          width: window.physicalSize.width,
          alignment: Alignment.center,
          height: 120,
          color: Colors.grey,
      );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          '上金谷欢迎您！',
          style: TextStyle(fontSize: 16.0, color: Colors.black),
        ),
        centerTitle: true,
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
        child: GestureDetector(
           onTap: widget.isLogin == false ?  (){
              Navigator.pushNamed(context, '/login');
           } : null,
          child: Padding(
            padding: EdgeInsets.all(10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('周五/03月27日',
                              style: TextStyle(
                                fontSize: 14.0,
                                color: ColorsUtil.mainColor,
                              )),
                          Text(
                            '2020',
                            style: TextStyle(fontSize: 16.0, color: Colors.black),
                          )
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        verticalDirection: VerticalDirection.down,
                        children: [
                          Container(
                            width: 75,
                            height: 75,
                            decoration: BoxDecoration(
                                color: Colors.grey,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(50.0))),
                          ),
                          SizedBox(height: 10.0,),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [Text(widget.isLogin == false ? '未登录' : ''), Text('>')],
                          )
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text('周五/03月27日',
                              style: TextStyle(
                                fontSize: 14.0,
                                color: ColorsUtil.mainColor,
                              )),
                          Text(
                            '2020',
                            style: TextStyle(fontSize: 16.0, color: Colors.black),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
                Text('景区服务',style: TextUtils.titleStyle,),
                _createView(context),
                ADBanner(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}


