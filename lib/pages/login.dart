import 'dart:collection';

import 'package:app_sjg/utils/color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorsUtil.appBarColor,
      appBar: AppBar(
        backgroundColor: ColorsUtil.appBarColor,
        elevation: 0.0,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios_outlined,
              color: Colors.grey[900],
            ),
            onPressed: () {
              Navigator.pop(context);
            }),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              width: 90,
              height: 90,
              margin: EdgeInsets.only(top: 70, bottom: 10.0),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: ColorsUtil.mainColor,
                  borderRadius: BorderRadius.all(Radius.circular(10.0))),
            ),
            Center(
                child: Text(
              '上金谷',
              style: TextStyle(fontSize: 16.0),
            )),
            Container(
              padding: EdgeInsets.only(left: 20.0),
              margin: EdgeInsets.only(left: 20.0,right: 20.0,top: 20.0),
              decoration: BoxDecoration(
                  color: Colors.grey[200],
                  borderRadius: BorderRadius.horizontal(
                      left: Radius.circular(20.0),
                      right: Radius.circular(20.0))),
              child: Row(
                children: [
                  Expanded(
                    child: TextField(
                      keyboardType: TextInputType.phone,
                      maxLines: 1,
                      maxLength: 11,
                      cursorColor: Colors.grey[300],
                      decoration: InputDecoration(hintText:'输入手机号',border: InputBorder.none,
                      hintStyle: TextStyle(color: Colors.grey[300]),counterText: ""),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: 20.0),
              margin: EdgeInsets.only(left: 20.0,right: 20.0,top: 20.0),
              decoration: BoxDecoration(
                  color: Colors.grey[200],
                  borderRadius: BorderRadius.horizontal(
                      left: Radius.circular(20.0),
                      right: Radius.circular(20.0))),
              child: Row(
                children: [
                  Expanded(
                    child: TextField(
                      keyboardType: TextInputType.number,
                      maxLines: 1,
                      cursorColor: Colors.grey[300],
                      decoration: InputDecoration(hintText:'输入验证码',border: InputBorder.none,
                      hintStyle: TextStyle(color: Colors.grey[300])),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(height: 60.0,),
            RaisedButton(onPressed: (){
               Navigator.pushNamed(context, '/guide',arguments: true);
            },
              color: ColorsUtil.mainColor,
              shape: StadiumBorder(),
              elevation: 0.0,
              padding: EdgeInsets.only(left: 150.0,right: 150.0,top: 10.0,bottom: 10.0),
            child: Text('登录/注册',style: TextStyle(color: Colors.white,fontSize: 16.0),),)
          ],
        ),
      ),
    );
  }
}
