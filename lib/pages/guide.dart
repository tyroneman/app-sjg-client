import 'dart:ui';

import 'package:app_sjg/pages/consecrate.dart';
import 'package:app_sjg/pages/home.dart';
import 'package:app_sjg/pages/mine.dart';
import 'package:app_sjg/utils/color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class GuidePage extends StatefulWidget {
      final  bool  isLogin;

     GuidePage({this.isLogin = false});


  @override
  _GuidePageState createState() => _GuidePageState();
}

class _GuidePageState extends State<GuidePage>
    with SingleTickerProviderStateMixin {
  int i;
  int _pageIndex = 0;
  PageController controller = PageController();
  GlobalKey<_GuidePageState> _pageIndicatorKey = GlobalKey();
  TabController tabController;
  int currentIndex = 0; //当前页面

  startview() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    bool isFirst = preferences.getBool('first');

    if (isFirst != null && !isFirst) {
      //not first
      setState(() {
        i = 1;
      });
    } else {
      preferences.setBool('first', false);
      setState(() {
        i = 0;
      });
    }
  }

  _dotWidget(int index) {
    return Container(
        width: 10,
        height: 10,
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.all(color: Colors.black, width: 1.0),
            color: (_pageIndex == index) ? Colors.black : Colors.transparent));
  }

  @override
  void initState() {
    startview();
    tabController = new TabController(length: 3, vsync: this);
    super.initState();
  }

  Widget _createPageView() {
    return PageView(
      controller: controller,
      onPageChanged: (pageIndex) {
        setState(() {
          _pageIndex = pageIndex;
          print(controller.page);
        });
      },
      children: [
        Container(
          color: Colors.blue,
          child: Center(child: Text('第一页')),
        ),
        Container(
          color: Colors.red,
          child: Center(child: Text('第二页')),
        ),
        Container(
          color: Colors.blueAccent,
          child: Align(
            alignment: Alignment(0, 0.5),
            child: GestureDetector(
                onTap: () {
                  setState(() {
                    i = 1;
                  });
                },
                child: Text('立即体验')),
          ),
        ),
      ],
    );
  }

  _handlePageIndicatorTap(TapUpDetails detail) {
    RenderBox renderBox = _pageIndicatorKey.currentContext.findRenderObject();
    Size widgeSize = renderBox.paintBounds.size;
    Offset tapOffset = renderBox.globalToLocal(detail.globalPosition);

    if (tapOffset.dx > widgeSize.width / 2) {
      _scrollToNextPage();
    } else {
      _scrollToPreviousPage();
    }
  }

  _scrollToPreviousPage() {
    if (_pageIndex > 0) {
      controller.animateToPage(_pageIndex - 1,
          duration: Duration(milliseconds: 200), curve: Curves.ease);
    }
  }

  _scrollToNextPage() {
    if (_pageIndex > 0) {
      controller.animateToPage(_pageIndex + 1,
          duration: Duration(milliseconds: 200), curve: Curves.ease);
    }
  }

  _createPageIndicator() {
    return Opacity(
      opacity: 0.7,
      child: Align(
        alignment: FractionalOffset.bottomCenter,
        child: Container(
          margin: EdgeInsets.only(bottom: 60),
          height: 40,
          width: 80,
          padding: EdgeInsets.all(0),
          decoration: BoxDecoration(
              color: Colors.transparent, //.withAlpha(128),
              borderRadius: BorderRadius.all(const Radius.circular(6.0))),
          child: GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTapUp: (detail) => _handlePageIndicatorTap(detail),
            child: Row(
                key: _pageIndicatorKey,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  _dotWidget(0),
                  _dotWidget(1),
                  _dotWidget(2),
                ]),
          ),
        ),
      ),
    );
  }

//  切换页面
  void _changePage(int index) {
    if (index != currentIndex) {
      setState(() {
        currentIndex = index;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    if (i == 0) {
      return Scaffold(
        body: Container(
          //引导页
          color: Colors.white,
          child: Stack(
            children: [
              _createPageView(),
              Align(
                  alignment: Alignment.topRight,
                  child: GestureDetector(
                    onTap: () {
                      setState(() {
                        i = 1;
                      });
                    },
                    child: GestureDetector(
                        onTap: () {
                          setState(() {
                            i = 1;
                          });
                        },
                        behavior: HitTestBehavior.translucent,
                        child: Padding(
                          padding: EdgeInsets.only(top: 50, right: 30),
                          child: Text(
                            '跳过',
                            style:
                                TextStyle(fontSize: 16.0, color: Colors.black),
                          ),
                        )),
                  )),
              _createPageIndicator()
            ],
          ),
        ),
      );
    } else {
      return Scaffold(
        body: TabBarView(
          children: [HomePage(widget.isLogin), ConsecratePage(), MinePage()],
          controller: tabController,
        ),
        bottomNavigationBar: Material(
          color: Colors.white,
          child: TabBar(
            controller: tabController,
            indicatorColor: ColorsUtil.mainColor,
            unselectedLabelColor: Colors.black,
            labelColor: ColorsUtil.mainColor,
            indicatorSize: TabBarIndicatorSize.label,
            indicatorPadding: EdgeInsets.only(bottom: 3.0),
            tabs: [
              Tab(
                text: '首页',
              ),
              Tab(
                text: '供奉',
              ),
              Tab(
                text: '我的',
              )
            ],
            indicatorWeight: 3.0,
          ),
        ),
      );
    }
  }
}
